/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuenta;
import java.util.Scanner;
/**
 *
 * @author Ximena Puchaicela
 */
class Cuenta {
    private double saldo;
    public Cuenta(double saldo_inicial){
        this.setSaldo(saldo_inicial);
    }
    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {
        this.saldo = (saldo<0)?0:saldo;
    }
    public void Abonar(double monto){
        this.saldo =(monto>0)?this.saldo+monto:this.saldo;
    }
    public void Cargar(double retiro){
        if(retiro >this.saldo) 
            System.out.println("Excedio el monto");
        else 
            this.saldo-=retiro;   
    }
}
public class PruebaCuenta {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        double monto,retiro;
        Cuenta cuenta1= new Cuenta(12.40);
        Cuenta cuenta2= new Cuenta(-12.40);
        System.out.format(" Saldo de cuenta:  %.2f\n",cuenta1.getSaldo()) ;
        System.out.format(" Saldo de cuenta:  %.2f\n",cuenta2.getSaldo()) ;
        System.out.print("Ingrese el valor a depositar: ");monto = teclado.nextDouble();
        cuenta1.Abonar(monto);
        System.out.println("Su saldo total es : "+cuenta1.getSaldo());
        System.out.print("Ingrese el valor a depositar: ");monto = teclado.nextDouble();
        cuenta2.Abonar(monto);
        System.out.println("Su saldo total es : "+cuenta2.getSaldo());
        System.out.print("Ingrese el valor a retirar de su cuenta: ");retiro = teclado.nextDouble();
        cuenta1.Cargar(retiro);
        System.out.format("Su saldo actual es : %.2f",cuenta1.getSaldo());
        System.out.println("\nIngrese el valor a retirar de su cuenta: ");retiro = teclado.nextDouble();
        cuenta2.Cargar(retiro);
        System.out.format("Su saldo actual es : %.2f ",cuenta2.getSaldo());
        
    }
}
